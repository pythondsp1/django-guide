.. Django Guide documentation master file, created by
   sphinx-quickstart on Wed Feb  7 16:43:32 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Django Guide (version 1.11.10)
==============================

.. note:: 

   * Download :download:`registration-html-files with crispy-tags <django/zip_codes/registration.tar.gz>` for easy registration which is discussed in :numref:`sec_crispy_form_t` and :numref:`sec_django_regis_redux_x`. Copy the extracted folder 'registration' inside the 'templates' folder in the root-folder (i.e. **templates/registration/html-files**). 

   * The webpage created in this tutorial can be seen at "http://djangoguide.pythonanywhere.com/". 

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Contents:

   django/basic
   django/admin
   django/view
   django/search
   django/modelform
   django/deploy