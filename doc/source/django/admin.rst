Admin page
**********


List views
==========

In this section, we will see various list-views available for the Django-admin page. List views are the views which shows the 'multiple items' at once.

Model registration method
-------------------------

The 'admin.ModelAdmin' is used to change the look of the models in the 'admin' page. For this we need to create a class (Lines 9-10 and 13-15) which inherits the 'admin.ModelAdmin', and then register it admin.site'. 

There are two ways to register; 

* First, register directly in the 'admin.site.register (Line 18), 
* Second method uses the decorator (Line 13). Note that we commented the Line 19 as 'Book' is already register using decorator. 


.. code-block:: python
    :linenos:
    :emphasize-lines: 9-10, 13-15, 18-19

    # books/admin.py

    from django.contrib import admin

    # Register your models here.
    from .models import Author, Book, BookImage, Publisher


    class AuthorAdmin(admin.ModelAdmin):
        pass


    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        pass


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    admin.site.register(Publisher)



Modify the listed-fields in 'Author' page
-----------------------------------------

Let's add an Author as shown in :numref:`fig_admin1`. After add the author the 'author page' will look as shown in :numref:`fig_admin2`

.. _`fig_admin1`:

.. figure:: fig/admin/fig_admin1.png

   Add an author


.. _`fig_admin2`:

.. figure:: fig/admin/fig_admin2.png

   Author page

* Now modify the admin page as below. The Line 10 uses three fields (and exclude one i.e. middle_name), therefore three columns will be displayed on the 'admin page' as shown in :numref:`fig_admin3`. 

.. code-block:: python
    :linenos:
    :emphasize-lines: 10

    # books/admin.py

    from django.contrib import admin

    # Register your models here.
    from .models import Author, Book, BookImage, Publisher


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']



    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        pass


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    admin.site.register(Publisher)


.. _`fig_admin3`:

.. figure:: fig/admin/fig_admin3.png

   First name, last name and email in 'admin page'



Ordering
--------


We can select the column for 'ordering' the items, as shown in Line 14, which will sort the items in ascending order. For this add some more names in the 'Author' and the modify the admin page as below; the output is shown in :numref:`fig_admin4`,

.. code-block:: python
    :linenos:
    :emphasize-lines: 11-12

    # books/admin.py

    from django.contrib import admin

    # Register your models here.
    from .models import Author, Book, BookImage, Publisher


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email


    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        pass


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    admin.site.register(Publisher)


.. _`fig_admin4`:

.. figure:: fig/admin/fig_admin4.png

   First name, last name and email in 'admin page'


.. note:: 

    Note that, the ordering can be changes through 'models.py' as well, but that will affect the database as well,

    .. code-block:: python
        :linenos: 

        # books/models.py

        class Author(models.Model):
            [...]

        class Meta:
            ordering = ['last_name', 'email'] # first by last_name then by email



Properties
----------

We can change the 'admin-name' (i.e. name of first column) using the property in the models.py file. For this first create a publisher with following details, 

.. code-block:: text

    Name    : Oxford
    Website : www.oxford.com


The entry will be displayed as shown in :numref:`fig_admin5`. Note that publisher name is not displayed in the figure as we did not add the 'str' in the class 'Publisher'. 

.. _`fig_admin5`:

.. figure:: fig/admin/fig_admin5.png

   Name of Publisher is not shown 


* In the below code, 'str (Lines 8-9)' is used to display the name of the first column in the admin-page. Then Lines 11-13 modifies that name as 'name (website)'

.. code-block:: python
    :linenos: 
    :emphasize-lines: 8-9, 11-13

    class Publisher(models.Model):
        name    = models.CharField(max_length=50)
        address = models.CharField(max_length=50, blank=True, null=True)
        city    = models.CharField(max_length=50, blank=True, null=True)
        country = models.CharField(max_length=50, blank=True, null=True)
        website = models.URLField()

        def __str__(self): # used as admin-name i.e. first column 
            return self.name

        @property  # change admin name to 'name (website)'
        def admin_name(self):
            return "%s (%s)" % (name, website)

* Next we need to register the 'admin_name' in the 'admin.py' as shown below, 

.. code-block:: python
    :linenos: 
    :emphasize-lines: 19-21, 26

    # books/admin.py

    from django.contrib import admin

    # Register your models here.
    from .models import Author, Book, BookImage, Publisher


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email


    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        pass

    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py

    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)

* Now see the admin page, the name will be displayed as "**Oxford (http://www.oxford.com)**". Note that, we set the website name as 'www.oxford.com' and the **http** is added automatically as the field is 'URLField'. 

Editable fields
---------------

We can make fields editable in the **List** page (without going to details-page). This is quite hand in various cases e.g. changing book to 'draft to no-draft' and 'not shipped to shipped' etc. 

Lets add some books in the 'Book', then modify the 'admin.py' as below; the corresponding 'admin-page' is shown in :numref:`fig_admin6`

.. code-block:: python
    :linenos: 
    :emphasize-lines: 15-19

    # books/admin.py

    from django.contrib import admin

    # Register your models here.
    from .models import Author, Book, BookImage, Publisher


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email


    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'draft']
        list_editable = ['draft']

    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py

    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)


.. _`fig_admin6`:

.. figure:: fig/admin/fig_admin6.png

   Editable 'draft' in list view


Filter the fields
-----------------

We can add the filter-option as well as shown in Line 13; and the corresponding figure is shown in :numref:`fig_admin7`. 

.. code-block:: python
    :linenos: 
    :emphasize-lines: 13

    # books/admin.py

    from django.contrib import admin

    # Register your models here.
    from .models import Author, Book, BookImage, Publisher


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email
        list_filter = ['last_name']


    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'draft']
        list_editable = ['draft']

    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py

    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)


.. _`fig_admin7`:

.. figure:: fig/admin/fig_admin7.png

   Filter in admin page



Search fields
-------------

We can add the search field using 'search_fields' as shown in Line 22; and corresponding figure is shown in :numref:`fig_admin8`


.. code-block:: python
    :linenos: 
    :emphasize-lines: 22

    # books/admin.py

    from django.contrib import admin

    # Register your models here.
    from .models import Author, Book, BookImage, Publisher


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email
        list_filter = ['last_name']



    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'draft']
        list_editable = ['draft']
        search_fields = ['title']


    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)


.. _`fig_admin8`:

.. figure:: fig/admin/fig_admin8.png

   Search in admin page


Searching in multiple Classes
-----------------------------

We can search for multiple fields of different classes as well, e.g. we can search in 'publisher\_\_country' and 'author\_\_last_name' (using \_\_'), as shown in Line 37. Now, the search will occur in two fields i.e. 'publisher-country' and 'author-last_name' as shown in :numref:`fig_admin9`. 

.. code-block:: python
    :linenos: 
    :emphasize-lines: 22

    # books/admin.py

    from django.contrib import admin

    # Register your models here.
    from .models import Author, Book, BookImage, Publisher


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email
        list_filter = ['last_name']



    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'draft']
        list_editable = ['draft']
        search_fields = ['title', 'publisher__country', 'authors__last_name']


    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)

.. _`fig_admin9`:

.. figure:: fig/admin/fig_admin9.png

   Search in different classes : Flask's author is 'Krishna Patel'


Action fields
-------------

We can modify the action fields as shown in below listing (see Lines 10-15). Here, the 'draft status' of all the marked items are changed to 'True'. The action field is shown in

.. code-block:: python
    :linenos: 
    :emphasize-lines: 4, 10-15, 29, 32

    # books/admin.py

    from django.contrib import admin
    from django.utils import timezone

    # Register your models here.
    from .models import Author, Book, BookImage, Publisher


    def draft_status(modeladmin, request, queryset):
        queryset.update(
            draft=False,
            publication_date=timezone.now()
        )
    draft_status.short_description = 'Mark book as published now' # at admin page


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email
        list_filter = ['last_name']



    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'publication_date', 'draft']
        list_editable = ['draft']
        search_fields = ['title', 'publisher__country', 'authors__last_name']
        actions = [draft_status]


    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)


.. _`fig_admin10`:

.. figure:: fig/admin/fig_admin10.png

   Action field : Mark book as published now


Date hierarchy
--------------

We can add the date hierarchy as well to see the books based on the dates as shown in Line 34, and corresponding figure is shown :numref:`fig_admin11`


.. code-block:: python
    :linenos: 
    :emphasize-lines: 34

    # books/admin.py

    from django.contrib import admin
    from django.utils import timezone

    # Register your models here.
    from .models import Author, Book, BookImage, Publisher


    def draft_status(modeladmin, request, queryset):
        queryset.update(
            draft=False,
            publication_date=timezone.now()
        )
    draft_status.short_description = 'Mark book as published now' # at admin page


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email
        list_filter = ['last_name']



    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'publication_date', 
                                'draft', 'updated']
        list_editable = ['draft']
        search_fields = ['title', 'publisher__country', 'authors__last_name']
        actions = [draft_status]
        date_hierarchy = 'publication_date'

    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)


.. _`fig_admin11`:

.. figure:: fig/admin/fig_admin11.png

   Date hierarchy




Detail views
============

In this section, we will see various detail-views available for the Django-admin page. Detail views are the views which shows the 'details of one item' only as shown in :numref:`fig_admin12`

.. _`fig_admin12`:

.. figure:: fig/admin/fig_admin12.png

   Detail page


Fields
------

In :numref:`fig_admin12`, we can see the severals details of the product. We can limit the number of details in the admin-page as shown in Line 36. The modified detail page is shown in :numref:`fig_admin13`. 

.. note:: 

    Non modifiable fields, e.g. timestamp and updated, can not be added in the 'fields' . 

.. code-block:: python
    :linenos: 
    :emphasize-lines: 36

    # books/admin.py

    from django.contrib import admin
    from django.utils import timezone

    # Register your models here.
    from .models import Author, Book, BookImage, Publisher


    def draft_status(modeladmin, request, queryset):
        queryset.update(
            draft=False,
            publication_date=timezone.now()
        )
    draft_status.short_description = 'Mark book as published now' # at admin page


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email
        list_filter = ['last_name']



    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'publication_date', 
                                'draft', 'updated']
        list_editable = ['draft']
        search_fields = ['title', 'publisher__country', 'authors__last_name']
        actions = [draft_status]
        date_hierarchy = 'publication_date'

        fields = ['title', 'publisher']

    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)


.. _`fig_admin13`:

.. figure:: fig/admin/fig_admin13.png

   Modified detail page


Arrange fields in columns
-------------------------

We can arrange fields in columns for better readability (Lines 36-42 and :numref:`fig_admin14`). Note that, ('title', 'slug') are in one row, then 'content' in third row etc.


.. code-block:: python
    :linenos:
    :emphasize-lines: 36-42

    # books/admin.py

    from django.contrib import admin
    from django.utils import timezone

    # Register your models here.
    from .models import Author, Book, BookImage, Publisher


    def draft_status(modeladmin, request, queryset):
        queryset.update(
            draft=False,
            publication_date=timezone.now()
        )
    draft_status.short_description = 'Mark book as published now' # at admin page


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email
        list_filter = ['last_name']



    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'publication_date', 
                                    'draft', 'updated']
        list_editable = ['draft']
        search_fields = ['title', 'publisher__country', 'authors__last_name']
        actions = [draft_status]
        date_hierarchy = 'publication_date'

        fields = [
                ('title', 'slug'),
                ('authors', 'publisher'),
                'content', 
                # 'timestamp/updated' can not be used as it is non-modifiable 
                ('draft', 'publication_date')
            ]

    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)

.. _`fig_admin14`:

.. figure:: fig/admin/fig_admin14.png

   Modified detail page with columns


Field-sets
----------

We can have more control over the modifying the detail-page using 'fieldsets' as shown in Lines 50-11; and the corresponding figure is shown :numref:`fig_admin15`. 

.. note::

    Both 'fields' and 'fieldsets' can not be defined together. 

.. code-block:: python
    :linenos: 
    :emphasize-lines:  37-52

    # books/admin.py

    from django.contrib import admin
    from django.utils import timezone

    # Register your models here.
    from .models import Author, Book, BookImage, Publisher


    def draft_status(modeladmin, request, queryset):
        queryset.update(
            draft=False,
            publication_date=timezone.now()
        )
    draft_status.short_description = 'Mark book as published now' # at admin page


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email
        list_filter = ['last_name']



    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'publication_date', 
                                    'draft', 'updated']
        list_editable = ['draft']
        search_fields = ['title', 'publisher__country', 'authors__last_name']
        actions = [draft_status]
        date_hierarchy = 'publication_date'

        # both 'fields' and 'fieldsets' can not be specified together
        fieldsets = (
                (None, {  # label 1: None
                    'fields': ( # dictionary
                        ('title', 'slug'),
                        
                    )
                }),
                ('More details', { # under label 2 : More details
                    'classes': ('collapse',),  # css-class : minimized
                    'fields': (
                        ('authors', 'publisher'),
                        'content',
                        ('draft', 'publication_date'),
                    )
                })
            )

    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)




.. _`fig_admin15`:

.. figure:: fig/admin/fig_admin15.png

   Fields-set with collapse option 



Custom forms
============

Create form.py
--------------

We can modify the admin-forms to add the data into the database by using the file 'forms.py' and importing it to 'admin.py' as shown below. The corresponding form is shown in :numref:`fig_admin16` where 'email' is at the top, 

.. code-block:: python
    :linenos: 
    :caption: Create forms.py
    :name: py_form_py_cr

    # books/forms.py

    from django import forms

    from .models import Author


    class AuthorForm(forms.ModelForm):
        class Meta:
            fields = ['email', 'first_name', 'middle_name', 'last_name']
            model = Author


.. code-block:: python
    :linenos: 
    :emphasize-lines: 7, 23

    # books/admin.py

    from django.contrib import admin
    from django.utils import timezone

    # Register your models here.
    from .forms import AuthorForm
    from .models import Author, Book, BookImage, Publisher

    def draft_status(modeladmin, request, queryset):
        queryset.update(
            draft=False,
            publication_date=timezone.now()
        )
    draft_status.short_description = 'Mark book as published now' # at admin page


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email
        list_filter = ['last_name']
        form = AuthorForm



    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'publication_date', 
                                    'draft', 'updated']
        list_editable = ['draft']
        search_fields = ['title', 'publisher__country', 'authors__last_name']
        actions = [draft_status]
        date_hierarchy = 'publication_date'

        # both 'fields' and 'fieldsets' can not be specified together
        fieldsets = (
                (None, {  # label 1: None
                    'fields': ( # dictionary
                        ('title', 'slug'),
                        
                    )
                }),
                ('More details', { # under label 2 : More details
                    'classes': ('collapse',),  # css-class : minimized
                    'fields': (
                        ('authors', 'publisher'),
                        'content',
                        ('draft', 'publication_date'),
                    )
                })
            )

    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)


.. _`fig_admin16`:

.. figure:: fig/admin/fig_admin16.png

   Modified form : email is at the top


Add 'save as new' button in form
--------------------------------

Add Line 24 to enable the 'save as new' option for existing data. Also, add Line 25 to show the save-links on the top as well (by default at the bottom only). :numref:`fig_admin17` is the modified admin page.

.. code-block:: python
    :linenos: 
    :emphasize-lines: 24-25

    # books/admin.py

    from django.contrib import admin
    from django.utils import timezone

    # Register your models here.
    from .forms import AuthorForm
    from .models import Author, Book, BookImage, Publisher

    def draft_status(modeladmin, request, queryset):
        queryset.update(
            draft=False,
            publication_date=timezone.now()
        )
    draft_status.short_description = 'Mark book as published now' # at admin page


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email
        list_filter = ['last_name']
        form = AuthorForm
        save_as = True   # enable save as option
        save_on_top = True  # show the save-buttons on top and bottom



    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'publication_date', 
                                    'draft', 'updated']
        list_editable = ['draft']
        search_fields = ['title', 'publisher__country', 'authors__last_name']
        actions = [draft_status]
        date_hierarchy = 'publication_date'

        # both 'fields' and 'fieldsets' can not be specified together
        fieldsets = (
                (None, {  # label 1: None
                    'fields': ( # dictionary
                        ('title', 'slug'),
                        
                    )
                }),
                ('More details', { # under label 2 : More details
                    'classes': ('collapse',),  # css-class : minimized
                    'fields': (
                        ('authors', 'publisher'),
                        'content',
                        ('draft', 'publication_date'),
                    )
                })
            )

    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)




.. _`fig_admin17`:

.. figure:: fig/admin/fig_admin17.png

   'Save as new' and other buttons at the top


Modify 'models.py'
------------------


Before moving further, let's add one more field (i.e. age) in the model 'Author',

.. code-block:: python
    :linenos: 
    :emphasize-lines:  6-10, 16

    # books/models.py

    from django.db import models
    from django.utils.text import slugify
     
    AGE_GROUP = (
        (0, " < 13 "), 
        (1, " 13-19 "),
        (2, " > 19 ")
    )
        
    class Author(models.Model):
        first_name = models.CharField(max_length=50)
        middle_name = models.CharField(max_length=50, blank=True, null=True)
        last_name = models.CharField(max_length=50)
        age = models.IntegerField(choices=AGE_GROUP, default=2) # > 19
        email = models.EmailField()

        def __str__(self):
            return "%s, %s" % (self.last_name, self.first_name)

    class Publisher(models.Model):
        name    = models.CharField(max_length=50)
        address = models.CharField(max_length=50, blank=True, null=True)
        city    = models.CharField(max_length=50, blank=True, null=True)
        country = models.CharField(max_length=50, blank=True, null=True)
        website = models.URLField()

        def __str__(self): # used as admin-name i.e. first column 
            return self.name

        @property  # change admin name to 'name (website)'
        def admin_name(self):
            return "%s (%s)" % (self.name, self.website)

    class Book(models.Model):
        title       = models.CharField(max_length=100)
        slug        = models.SlugField(unique=True)
        authors     = models.ManyToManyField(Author)
        publisher   = models.ForeignKey(Publisher)
        content     = models.TextField()
        draft       = models.BooleanField(default=True)
        publication_date = models.DateField(null=True, blank=True)
        # verbose_name can be defined in following two ways
        timestamp   = models.DateTimeField( auto_now=False, 
                                            auto_now_add=True,
                                            verbose_name = "Created on"
                                        )
        updated     = models.DateTimeField( "Last updated", 
                                            auto_now=True, 
                                            auto_now_add=False
                                        )
        def __str__(self):
            return self.title


    # upload location : upload/book/title/slug.jpg
    def upload_location(instance, filename):
        title = slugify(instance.book.title) # Meher Baba -> Meher-Baba
        slug = instance.book.slug
        name, extension = filename.split(".")
        new_name = "%s.%s" % (slug, extension)
        return "books/%s/%s" % (title, new_name)

    class BookImage(models.Model):
        book    = models.ForeignKey(Book)
        image   = models.ImageField(upload_to=upload_location, 
                                    null=True, 
                                    blank=True
                                )

        def __str__(self): # self.image is location
            return "%s (%s)" % (self.book.title, self.image)                      

Next, run the migration commands, 

.. code-block:: shell

    $ python manage.py makemigrations
    $ python manage.py migrate


Then, add this new field in the forms.py, as we are using forms for model 'Book' (not the 'fields'). Now, see the admin page again and the age group will be added to it with default value of '\> 19' as shown in :numref:`fig_admin18`

.. code-block:: python
    :linenos:
    
    # books/forms.py

    from django import forms

    from .models import Author


    class AuthorForm(forms.ModelForm):
        class Meta:
            fields = ['first_name', 'middle_name', 'last_name', 'email', 'age']
            model = Author

.. _`fig_admin18`:

.. figure:: fig/admin/fig_admin18.png

   'Dropdown button' for IntegerField 'age'


Radio button
------------

Currently, features are shown as 'drop-down' button, which can be changed to 'radio button' as below. The admin page is shown in :numref:`fig_admin19`

.. code-block:: python
    :linenos: 
    :emphasize-lines: 26 

    # books/admin.py

    from django.contrib import admin
    from django.utils import timezone

    # Register your models here.
    from .forms import AuthorForm
    from .models import Author, Book, BookImage, Publisher

    def draft_status(modeladmin, request, queryset):
        queryset.update(
            draft=False,
            publication_date=timezone.now()
        )
    draft_status.short_description = 'Mark book as published now' # at admin page


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email
        list_filter = ['last_name']
        form = AuthorForm
        save_as = True   # enable save as option
        save_on_top = True  # show the save-buttons on top and bottom
        radio_fields = {'age': admin.HORIZONTAL} # admin.VERTICAL



    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'publication_date', 
                                    'draft', 'updated']
        list_editable = ['draft']
        search_fields = ['title', 'publisher__country', 'authors__last_name']
        actions = [draft_status]
        date_hierarchy = 'publication_date'

        # both 'fields' and 'fieldsets' can not be specified together
        fieldsets = (
                (None, {  # label 1: None
                    'fields': ( # dictionary
                        ('title', 'slug'),
                        
                    )
                }),
                ('More details', { # under label 2 : More details
                    'classes': ('collapse',),  # css-class : minimized
                    'fields': (
                        ('authors', 'publisher'),
                        'content',
                        ('draft', 'publication_date'),
                    )
                })
            )

    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)


.. _`fig_admin19`:

.. figure:: fig/admin/fig_admin19.png

   Radio buttons


Change look of 'list selection' (many-to-many field)
----------------------------------------------------

Compare the 'Author-field' in :numref:`fig_admin20` with :numref:`fig_admin14` after adding the Line 39,

    
.. code-block:: python
    :linenos: 
    :emphasize-lines: 39, 46, 52

    # books/admin.py

    from django.contrib import admin
    from django.utils import timezone

    # Register your models here.
    from .forms import AuthorForm
    from .models import Author, Book, BookImage, Publisher

    def draft_status(modeladmin, request, queryset):
        queryset.update(
            draft=False,
            publication_date=timezone.now()
        )
    draft_status.short_description = 'Mark book as published now' # at admin page


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email
        list_filter = ['last_name']
        form = AuthorForm
        save_as = True   # enable save as option
        save_on_top = True  # show the save-buttons on top and bottom
        radio_fields = {'age': admin.HORIZONTAL} # admin.VERTICAL



    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'publication_date', 
                                    'draft', 'updated']
        list_editable = ['draft']
        search_fields = ['title', 'publisher__country', 'authors__last_name']
        actions = [draft_status]
        date_hierarchy = 'publication_date'
        filter_horizontal = ['authors']

        # both 'fields' and 'fieldsets' can not be specified together
        fieldsets = (
                (None, {  # label 1: None
                    'fields': ( # dictionary
                        ('title', 'slug'),
                        ('authors'),
                    )
                }),
                ('More details', { # under label 2 : More details
                    'classes': ('collapse',),  # css-class : minimized
                    'fields': (
                        ('publisher'),
                        'content',
                        ('draft', 'publication_date'),
                    )
                })
            )
        
    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)


.. _`fig_admin20`:

.. figure:: fig/admin/fig_admin20.png

   Modified view for multiple-items selection


Pre-populated fields
--------------------

n the below code, the 'slug' will be automatically filled by Line 40, as we type in the 'Name' field (see :numref:`fig_admin21`). Note that 'slug' is of type 'SlugField', therefore the words like 'the' and 'of' are chopped out from the name. 

.. code-block:: python
    :linenos: 
    :emphasize-lines: 40

    # books/admin.py

    from django.contrib import admin
    from django.utils import timezone

    # Register your models here.
    from .forms import AuthorForm
    from .models import Author, Book, BookImage, Publisher

    def draft_status(modeladmin, request, queryset):
        queryset.update(
            draft=False,
            publication_date=timezone.now()
        )
    draft_status.short_description = 'Mark book as published now' # at admin page


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email
        list_filter = ['last_name']
        form = AuthorForm
        save_as = True   # enable save as option
        save_on_top = True  # show the save-buttons on top and bottom
        radio_fields = {'age': admin.HORIZONTAL} # admin.VERTICAL



    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'publication_date', 
                                    'draft', 'updated']
        list_editable = ['draft']
        search_fields = ['title', 'publisher__country', 'authors__last_name']
        actions = [draft_status]
        date_hierarchy = 'publication_date'
        filter_horizontal = ['authors']
        prepopulated_fields = {'slug': ('title',)} 

        # both 'fields' and 'fieldsets' can not be specified together
        fieldsets = (
                (None, {  # label 1: None
                    'fields': ( # dictionary
                        ('title', 'slug'),
                        ('authors'),
                    )
                }),
                ('More details', { # under label 2 : More details
                    'classes': ('collapse',),  # css-class : minimized
                    'fields': (
                        ('publisher'),
                        'content',
                        ('draft', 'publication_date'),
                    )
                })
            )

    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)
    


.. _`fig_admin21`:

.. figure:: fig/admin/fig_admin21.png

   Slug is automatically filled based on 'Name'


Inline items
============

.. note::

    If a class-1 has the ForeignKey from class-2, then in the admin page of class-2 we can inherit the class-1-items. 


* In TabularInline, the fields of a model are show in one rows (Lines 29-31 and 67), as shown :numref:`fig_admin22`.

* In StackedInline, the fields of models are show in different rows Lines 34-36 and 73) , as shown :numref:`fig_admin23`.


.. code-block:: python
    :linenos: 
    :emphasize-lines: 29-31, 34-36, 67, 73

    # books/admin.py

    from django.contrib import admin
    from django.utils import timezone

    # Register your models here.
    from .forms import AuthorForm
    from .models import Author, Book, BookImage, Publisher

    def draft_status(modeladmin, request, queryset):
        queryset.update(
            draft=False,
            publication_date=timezone.now()
        )
    draft_status.short_description = 'Mark book as published now' # at admin page


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email
        list_filter = ['last_name']
        form = AuthorForm
        save_as = True   # enable save as option
        save_on_top = True  # show the save-buttons on top and bottom
        radio_fields = {'age': admin.HORIZONTAL} # admin.VERTICAL


    class BookImageInline(admin.TabularInline):
        model = BookImage
        extra = 2 # optional : show 2 items (default = 3)


    class BookInline(admin.StackedInline):
        model = Book
        extra = 1  # show only one item

    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'publication_date', 
                                    'draft', 'updated']
        list_editable = ['draft']
        search_fields = ['title', 'publisher__country', 'authors__last_name']
        actions = [draft_status]
        date_hierarchy = 'publication_date'
        filter_horizontal = ['authors']
        prepopulated_fields = {'slug': ('title',)} 

        # both 'fields' and 'fieldsets' can not be specified together
        fieldsets = (
                (None, {  # label 1: None
                    'fields': ( # dictionary
                        ('title', 'slug'),
                    )
                }),
                ('More details', { # under label 2 : More details
                    'classes': ('collapse',),  # css-class : minimized
                    'fields': (
                        ('authors'),
                        ('publisher'),
                        'content',
                        ('draft', 'publication_date'),
                    )
                })
            )
        inlines = [BookImageInline]


    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py
        # both 'fields' and 'fieldsets' can not be specified together
        inlines = [BookInline]


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)


.. _`fig_admin22`:

.. figure:: fig/admin/fig_admin22.png

   TabularInline 


.. _`fig_admin23`:

.. figure:: fig/admin/fig_admin23.png

   StackedInline


Templates
=========

We can change the look of the admin page as well. 

* For example, go to templates->admin and create a html file (base_site.html) with following contents. Now the heading will be changed to 'Bookstore Admin' as shown in Fig. 1.23.

.. code-block:: html

    <!-- templates/admin/base_site.html -->

    {% extends "admin/base.html" %}

    {% block title %}{{ title }} | {{ site_title|default:_('Django site admin') }}{% endblock %}

    {% block branding %}
    <h1 id="site-name"><a href="{% url 'admin:index' %}">Bookstore Admin</a></h1>
    {% endblock %}

    {% block nav-global %}{% endblock %}


.. _`fig_admin24`:

.. figure:: fig/admin/fig_admin24.png

   Rename the branding in admin page


* Next, go to templates->admin->books->bookimage (i.e. http://127.0.0.1:8000/admin/books/bookimage) and add the 'change_form.html' with following contents,


.. code-block:: html

    <!-- templates/admin/books/bookimage/change_form.html -->

    {% extends 'admin/change_form.html' %}

    {% block content %}
        <h1 style="color:red">It is better to change the image from 'book details'</h1>
        {{ block.super }}
    {% endblock %}

Next add a book (http://127.0.0.1:8000/admin/books/book/add/) with some images. And go to 'Book image admin' (http://127.0.0.1:8000/admin/books/bookimage/) and open the book. We will get a message as shown in :numref:`fig_admin25`

.. _`fig_admin25`:

.. figure:: fig/admin/fig_admin25.png

   Message "It is better ... " is displayed on modify-bookimage-page of admin.


Conclusion
==========

In :numref:`Chapter %s <ch_basic_setup>` we setup the Django environment for local and production server along with various templates. In this chapter, we learn to manage the admin page which is quite useful for the administrators. But this page can not be used for public-viewing. From the next chapter, we will create various views for public usage. More specifically, in the next chapter we will learn the 'function based views' and 'class based views'. 