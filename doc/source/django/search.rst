Search
******

.. note:: 


    In some of the listings, only the newly added codes are shown. Therefore the complete codes of this chapter is provided which can be  :download:`downloaded from here<zip_codes/codes-4.tar.gz>`. This will be useful if we have problems in understanding the 'partial part' of the code.   


Introduction
============

In this chapter we will see the different ways to search the items in the database. 


Search based on 'value' in URL
==============================

In :numref:`sec_read_para_url` we learn to read the 'pattern' from the URL. In this section, we will use that pattern to search the books which contains the word in the url. 

.. code-block:: python
    
    # books/views.py

    [...]

    # Search similar books 
    class SearchBooks(ListView):
        template_name = 'books/book_list.html'
        def get_queryset(self):
            val = self.kwargs.get("urlsearch")
            if val:
                queryset = Book.objects.filter(title__icontains=val)
            else:
                queryset = Book.objects.none()
            return queryset



.. code-block:: python

    # books/urls.py

    from django.conf.urls import url

    from .views import (    http_example,
            fexample, CExample,
            book_list, BookList,
            BookDetail,
            read_arg, read_kwarg, ReadArg, ReadKwarg,
            SearchBooks,
        )

    # f in the link name, is used for function based view, otherwise class based view

    urlpatterns = [
        [...]

        url(r'^searchfromurl/(?P<urlsearch>[\w-]+)/$', SearchBooks.as_view(),
                name="searchfromurl"), # search item received from url
    ]


Now open the below URL and it will show the list of books which contains 'ing' in the 'title' as displayed in :numref:`fig_search1`

.. code-block:: text

    http://127.0.0.1:8000/books/searchsimilar/ing/



.. _`fig_search1`: 

.. figure:: fig/search/fig_search1.png

   Search the books which contains 'ing' in the title



Use 'Q' for multiple queries
============================

The 'Q' can be used for searching at multiple location as shown below, 

.. code-block:: python
    
    # books/views.py

    [...]

    # Search books using Q
    class QSearchBooks(ListView):
        template_name = 'books/book_list.html'
        def get_queryset(self):
            val = self.kwargs.get("qurlsearch")
            if val:
                queryset = Book.objects.filter(
                    Q(title__icontains=val) 
                  | Q(authors__first_name__icontains=val)  # | is or
                ) # .distinct()  
                # distinct is used, otherwise same book will be listed 
                # twice if book has two authors
                queryset = queryset.distinct()
            else:
                queryset = Book.objects.none()
            return queryset


Next, add the view in the 'url'. The results are shown in :numref:`fig_search2`

.. note:: 

    The 'distinct()' is used in the listing, otherwise if a book has two authors then it will be displayed twice. 

.. code-block:: python

    # books/urls.py

    from django.conf.urls import url

    from .views import (    http_example,
            fexample, CExample,
            book_list, BookList,
            BookDetail,
            read_arg, read_kwarg, ReadArg, ReadKwarg,
            SearchBooks, QSearchBooks
        )

    # f in the link name, is used for function based view, otherwise class based view

    urlpatterns = [
        [...]

        url(r'^qsearchfromurl/(?P<qurlsearch>[\w-]+)/$', QSearchBooks.as_view(),
                    name="qsearchfromurl"), # search item received from url using Q
    ]


.. _`fig_search2`: 

.. figure:: fig/search/fig_search2.png

   Cheetah and Flask has 'ah' in 'title' and 'author name' respectively


HTML Forms
==========

In previous section, we used the value in the URL to search the data in the database. In this section, we will create HTML forms to search the data. In the other words, the value will be entered in the 'form' and then read by the 'view' to process it. 


* Note that the name 'q' is provided by the 'search_form.html' (:numref:`html_search_form_1`). 
* The 'request.GET.get' is used to read the "get" value sent by the form. 

.. code-block:: python
    :linenos: 

    # books/views.py

    [...]

    # link to search form
    class  BookSearch(TemplateView):
        template_name = 'books/search_form.html'  # 'template_name' is predefined

    class BookSearchResult(ListView):
        model = Book
        template_name = 'books/book_list.html'

        def get_queryset(self, *args, **kwargs):
            val = self.request.GET.get("q")
            if val:
                queryset = Book.objects.filter(
                    Q(title__icontains=val) |
                    Q(content__icontains=val)
                    ).distinct()
            else:
                queryset = Book.objects.none()
            return queryset

* Add these views in urls.py, 

.. code-block:: python

    # books/urls.py

    from django.conf.urls import url

    from .views import (    http_example,
            fexample, CExample,
            book_list, BookList,
            BookDetail,
            read_arg, read_kwarg, ReadArg, ReadKwarg,
            SearchBooks, QSearchBooks, BookSearch, BookSearchResult,
        )

    # f in the link name, is used for function based view, otherwise class based view

    urlpatterns = [
        [...]

        url(r'^booksearch/$', BookSearch.as_view(), 
                                name="booksearch"), # Book search
        url(r'^booksearchresult/$', BookSearchResult.as_view(), 
                                    name="booksearchresult"), # Book search
    ]


* Next creat a form for searching the book, 


.. code-block:: html
    :linenos: 
    :caption: Search form 
    :name: html_search_form_1

    <!-- books/templates/books/search_form.html -->

    {% extends "base.html" %}
    {% load staticfiles %}

    {% block content %}

        <div class="row">
        <div class="col-sm-offset-2 col-sm-4">
        <h3> Search page </h3>
        <form method="get" action='{% url "book:booksearchresult" %}'>
            <input class="form-control" type="text" placeholder="Search" name="q" >
            <br>
            <button class="btn btn-primary" type="submit">Submit</button>
        </form>
        </div>
        </div>

    {% endblock %}


* We can dd a search box in the navbar as below, and skip the search form in :numref:`html_search_form_1` 

.. code-block:: html
    :linenos: 
    :emphasize-lines: 32-37

    <!-- templates/navbar.html  -->

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="nav navbar-brand" href="/">Bookstore</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="/">Home</a></li>
            <!-- <li><a href="/books">All Books</a></li> -->

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header">Nav header</li>
                  <li><a href="#">Separated link</a></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
            </li>
          </ul>

          <form class="navbar-form pull-left" method="GET" action='{% url "book:booksearchresult" %}'>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Search Books" name="q">
              </div>
             <!--  <button type="submit" class="btn btn-default">Submit</button> -->
          </form>

        </div><!--/.nav-collapse 
        -->
      </div>
    </nav>


* Now, use 'navbar-search' or 'search form' to find the books, and the result will be shown as in :numref:`fig_search3`. 


.. _`fig_search3`: 

.. figure:: fig/search/fig_search3.png

   Search box


Conclusion
==========

In this chapter, we saw several ways for searching items. More specifically, we used the HTML-form for searching the books. In the next chapter, we will learn the Django forms along with some more class based views. 