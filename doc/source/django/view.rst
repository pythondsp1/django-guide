Views and Templates
*******************

.. note:: 

    In some of the listings, only the newly added codes are shown. Therefore the complete codes of this chapter is provided which can be  :download:`downloaded from here <zip_codes/codes-3.tar.gz>`. This will be useful if we have problems in understanding the 'partial part' of the code.       


Introduction
============

In previous chapters, we have created few models i.e. Book, Author and Publisher; and then we used the admin page to view and modify these models. But the admin page can not be shared for public viewing. In this chapter, we will create some views which will be publicly available to see the HTML pages.


Signals in models
=================

Lets add one more class to the 'models.py' which will store the information about the 'Format' of the book. Also modify the 'slug' field as shown in Line 48.  

.. note::

    The 'signals' are used in this model, which needs two input parameter i.e. 'function name' and 'model name' as shown in Lines 115 and 127. For example at Line 115, the 'pre_save' signal is declared which has the function 'pre_save_add_slug'; and this function will be called before saving the data of the model 'Book'.  

    Also, post_save signal contains a parameter 'created'. The input parameter for the function 'pre_save_add_slug' does not contain 'created' argument, whereas the function 'post_save_format_for_book' contains the 'created' argument. 


.. code-block:: python
    :linenos:
    :emphasize-lines: 48, 87-127

    # books/models.py

    from django.db import models
    from django.utils.text import slugify
    from django.db.models.signals import post_save, pre_save


    AGE_GROUP = (
        (0, "< 13"), 
        (1, "13-19"),
        (2, "> 19")
    )
        
    BOOK_FORMAT = (
        (0, "PDF"), 
        (1, "Hard Cover"),
        (2, "Epub"), 
        (3, "HTML")
    )


    class Author(models.Model):
        first_name = models.CharField(max_length=50)
        middle_name = models.CharField(max_length=50, blank=True, null=True)
        last_name = models.CharField(max_length=50)
        age = models.IntegerField(choices=AGE_GROUP, default=2) # > 19
        email = models.EmailField()

        def __str__(self):
            return "%s, %s" % (self.last_name, self.first_name)

    class Publisher(models.Model):
        name    = models.CharField(max_length=50)
        address = models.CharField(max_length=50, blank=True, null=True)
        city    = models.CharField(max_length=50, blank=True, null=True)
        country = models.CharField(max_length=50, blank=True, null=True)
        website = models.URLField()

        def __str__(self): # used as admin-name i.e. first column 
            return self.name

        @property  # change admin name to 'name (website)'
        def admin_name(self):
            return "%s (%s)" % (self.name, self.website)

    class Book(models.Model):
        title       = models.CharField(max_length=100)
        slug        = models.SlugField(unique=True, blank=True)
        authors     = models.ManyToManyField(Author)
        publisher   = models.ForeignKey(Publisher)
        content     = models.TextField()
        draft       = models.BooleanField(default=True)
        publication_date = models.DateField(null=True, blank=True)
        # verbose_name can be defined in following two ways
        timestamp   = models.DateTimeField( auto_now=False, 
                                            auto_now_add=True,
                                            verbose_name = "Created on"
                                        )
        updated     = models.DateTimeField( "Last updated", 
                                            auto_now=True, 
                                            auto_now_add=False
                                        )
        def __str__(self):
            return self.title


    # upload location : upload/book/title/slug.jpg
    def upload_location(instance, filename):
        title = slugify(instance.book.title) # Meher Baba -> Meher-Baba
        slug = instance.book.slug
        name, extension = filename.split(".")
        new_name = "%s.%s" % (slug, extension)
        return "books/%s/%s" % (title, new_name)

    class BookImage(models.Model):
        book    = models.ForeignKey(Book)
        image   = models.ImageField(upload_to=upload_location, 
                                    null=True, 
                                    blank=True
                                )

        def __str__(self): # self.image is location
            return "%s (%s)" % (self.book.title, self.image)



    class Format(models.Model):
        book        = models.ForeignKey(Book)
        book_format = models.IntegerField(choices=BOOK_FORMAT, default=0) # PDF
        price       = models.DecimalField(decimal_places=2, max_digits=10)
        sale_price  = models.DecimalField(decimal_places=2, max_digits=10, 
                                              blank=True, null=True)
        available   = models.BooleanField(default=True)
        inventory   = models.IntegerField(null=True, blank=True)

        def __str__(self):
            return "%s (%s)" % (self.book, BOOK_FORMAT[self.book_format][1])

        # get sale price if exists
        def get_price(self): 
            if sale_price is not None:
                return self.sale_price
            else:
                return self.price

    # pre_save does not have "created" argument as post_save
    def pre_save_add_slug(sender, instance, *args, **kwargs): 
        if not instance.slug:
            new_slug = slugify(instance.title)
            exists = Book.objects.filter(slug=new_slug).exists()
            if exists:
                new_slug = "%s-%s" % (new_slug, instance.id)
            instance.slug = new_slug

    pre_save.connect(pre_save_add_slug, sender=Book)


    def post_save_format_for_book(sender, instance, created, *args, **kwargs):
        formats = instance.format_set.all()
        if formats.count()==0:
            new_format = Format() # create object
            new_format.book = instance
            new_format.book_format = 0 # PDF
            new_format.price = 0.00
            new_format.save()

    post_save.connect(post_save_format_for_book, sender=Book)

* Now, run the migration commands, 

.. code-block:: text
    
    $ python manage.py makemigrations

    $ python manage.py migrate. 


* Next, register the new model in the admin.py. 


.. code-block:: python
    :linenos: 
    :emphasize-lines: 8, 82-86

    # books/admin.py

    from django.contrib import admin
    from django.utils import timezone

    # Register your models here.
    from .forms import AuthorForm
    from .models import Author, Book, BookImage, Format, Publisher

    def draft_status(modeladmin, request, queryset):
        queryset.update(
            draft=False,
            publication_date=timezone.now()
        )
    draft_status.short_description = 'Mark book as published now' # at admin page


    class AuthorAdmin(admin.ModelAdmin):
        list_display = ['first_name', 'last_name', 'email']
        # sort in ascending order
        ordering = ['last_name', 'email'] # first by last_name then by email
        list_filter = ['last_name']
        form = AuthorForm
        save_as = True   # enable save as option
        save_on_top = True  # show the save-buttons on top and bottom
        radio_fields = {'age': admin.HORIZONTAL} # admin.VERTICAL


    class BookImageInline(admin.TabularInline):
        model = BookImage
        extra = 2 # optional : show 2 items (default = 3)


    class BookInline(admin.StackedInline):
        model = Book
        extra = 1  # show only one item

    @admin.register(Book)
    class BookAdmin(admin.ModelAdmin):
        # note that 'many-to-many (i.e. authors)' field can not be displayed
        list_display = ['title', 'publisher', 'publication_date',
                                    'draft', 'updated']
        list_editable = ['draft']
        search_fields = ['title', 'publisher__country', 'authors__last_name']
        actions = [draft_status]
        date_hierarchy = 'publication_date'
        filter_horizontal = ['authors']
        prepopulated_fields = {'slug': ('title',)}

        # both 'fields' and 'fieldsets' can not be specified together
        fieldsets = (
                (None, {  # label 1: None
                    'fields': ( # dictionary
                        ('title', 'slug'),
                    )
                }),
                ('More details', { # under label 2 : More details
                    'classes': ('collapse',),  # css-class : minimized
                    'fields': (
                        ('authors'),
                        ('publisher'),
                        'content',
                        ('draft', 'publication_date'),
                    )
                })
            )
        inlines = [BookImageInline]


    @admin.register(Publisher)
    class PublisherAdmin(admin.ModelAdmin):
        list_display = ['admin_name']  # defined in models.py
        # both 'fields' and 'fieldsets' can not be specified together
        inlines = [BookInline]


    admin.site.register(Author, AuthorAdmin)
    # admin.site.register(Book)
    admin.site.register(BookImage)
    # admin.site.register(Publisher)

    @admin.register(Format)
    class FormatAdmin(admin.ModelAdmin):
        save_as = True   # enable save as option
        save_on_top = True  # show the save-buttons on top and bottom
        radio_fields = {'book_format': admin.HORIZONTAL} # admin.VERTICAL



* Next add some books as shown in :numref:`fig_view1`. 


.. _`fig_view1`:

.. figure:: fig/view/fig_view1.png

   Add some books and formats with price and sale price. 



Context in templates
====================

Before creating the views for the books, let's understand the 'context' in the templates with some examples. 

Please note the following points in the below code, 

* Line 8 returns a HttpResponse directly to the html page. 
* Line 13 contains the 'context' which is a dictionary in a **function** based view. These values are passed to the HMTL page by Line 17. 
* The 'get_context_data' (at Line 22) is the predefined method in the **class based view**. The 'TemplateView' is a type of class based view. Then value of context is return by Line 27 and passed to HTML page directly (we need not to make setup for this). Lastly, we can use any name in place of **context**; but we can **not renamed** the word 'template_name' in class based views.   
* The output is shown in :numref:`fig_view2`


.. _`fig_view2`:

.. figure:: fig/view/fig_view2.png

    Passing context to HTML

.. note:: 

    In TemplateView, we need to provide the location of "html files"; whereas in ListView and DetailView, the location can be rendered automatically. 

.. code-block:: python
    :linenos: 
    :emphasize-lines: 8, 12-13, 22

    # books/views.py

    from django.http import HttpResponse 
    from django.shortcuts import render
    from django.views.generic import TemplateView

    def http_example(request):
        return HttpResponse("<h3> This is HttpResponse </h3> <p> Thank You! </p>")


    def fexample(request):
        template_name = 'books/context_ex.html' # user defined name 'template_name'
        context = {                     # user defined name 'context'
            "name" : "Meher Krishna Patel",
            "fruits" : ["apple", "orange", "banana"]
        }
        return render(request, template_name, context)


    class  CExample(TemplateView):
        template_name = 'books/context_ex.html'  # 'template_name' is predefined
        def get_context_data(self, *args, **kwargs):
            context = {                     # user defined name 'context'
                    "name" : "Harry",
                    "fruits" : ["pear", "cherry", "berry"]
                }
            return context


* Below are the urls setup for above three views, 

.. code-block:: python
    :linenos: 

    # books/urls.py

    from django.conf.urls import url

    from .views import (    fexample,
            CExample, http_example,             
        )


    urlpatterns = [
        url(r'^httpExample/$', http_example, name="httpExample"), # HttpResponse
        url(r'^fexample/$', fexample, name="fexample"), # function-based-view
        url(r'^cexample/$', CExample.as_view(), name="cexample"), # class-based-view
    ]


* Since above urls are defined in the app 'books', therefore we need to include these urls in the main urls i.e. 'bookstore/urls.py' as shownin Line 15 of below code, 

.. code-block:: python
    :linenos:

    # Configuration-folder/urls.py

    from django.conf.urls import url, include
    from django.conf.urls.static import static
    from django.contrib import admin
    from django.conf import settings

    from .views import all_links 


    urlpatterns = [
        url(r'^admin/', admin.site.urls),
        url(r'^$', all_links, name="home"),  # home page

        url(r'^books/', include("books.urls", namespace="book")), # books app
    ]


    # required to see the media files in debug mode
    if settings.DEBUG:
        urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


* Below is the HTML code, which is used by both 'fexample' and 'CExample' views. 
  
.. code-block:: html

    <!-- books/templates/books/context_ex.html -->

    {% extends "base.html" %}
    {% load staticfiles %}

    {% block content %}

        <h2> Hello {{ name }} </h2>

        <p> Below are the good fruits </p>
        <ul>
        {% for fruit in fruits %}
            <li> {{ fruit }} </li>
        {% endfor %}
        </ul>

    {% endblock %}


.. important:: 

    Please notice the following points, 

    * The classed based views have several inbuilt method (e.g. get_context_data) which are easy to use and less error prone as compare to function based views. 
    * Also, there are certain variables which can not be renamed in the class based views e.g. template_name in the above example. 

    In this chapter, we will learn several inbuilt methods of class based view. 



List view
=========

Add below code at the end of the file 

* Here book_list and BookList are the function-based and class-based views respectively. 
* Note that 'queryset' is automatically passed as the 'context' in the class based views. Further *queryset or get_queryset* is **compulsory** to define in ListView. 
* Read comments for more details. 
* Output HTML is shown in :numref:`fig_view3`

.. note:: 

    The 'queryset' is automatically passed as the 'context' in ListView, therefore the new-context is pass as 'context["name"] = "Harry"'; if we try to pass as 'function based view', then a new context will be created which will not have the 'queryset-values'.  

.. _`fig_view3`:

.. figure:: fig/view/fig_view3.png

    List of books


.. code-block:: python
    :linenos:

    # books/views.py

    from django.http import HttpResponse 
    from django.shortcuts import render
    from django.views.generic import TemplateView, ListView

    [...]

    # display list of books using function based views
    def book_list(request): 
        template_name = "books/book_list.html"
        queryset = Book.objects.all()
        context = {
            "name" : "Meher Krishna Patel", 
            "object_list": queryset
        }
        return render(request, template_name, context)


    # display list of books using class based views
    class BookList(ListView): 
        # # default template location : lowercaseAppName/lowercaseAppName_list.html
        # template_name = "books/book_list.html"

        # by default 'queryset' is sent as context 
        queryset = Book.objects.all()  # 'queryset' is reserved name like 'template_name'

        # to pass more context, inherit cotext with 'super' and 
        # then include the items in the context  using 'get_context_data'
        def get_context_data(self, *args, **kwargs): 
            context = super().get_context_data(*args, **kwargs)
            context["name"] = "Harry"
            return context


Below is the HTML which is used by both the views, 

.. code-block:: html

    <!-- books/templates/books/book_list.html -->

    {% extends "base.html" %}
    {% load staticfiles %}

    {% block content %}

        <h2> Hello {{ name }} </h2>

        <h4> List of books </h4>

        <ul>
        {% for obj in object_list %}
            <li> {{ obj }} </li>
        {% endfor %}
        </ul>

    {% endblock %}


Add following urls for these new views, 

.. code-block:: python
    :linenos:

    # books/urls.py

    from django.conf.urls import url

    from .views import (    http_example,
            fexample, CExample,
            book_list, BookList             
        )

    # f in the link name, is used for function based view, otherwise class based view

    urlpatterns = [
        [...]

        url(r'^fbooklist/$', book_list, name="fbooklist"), # Book list
        url(r'^$', BookList.as_view(), name="booklist"), # Book list
    ]


Detail view with (ForeignKey and ManyToManyField)
=================================================

Add the code at the end of the file, 

* We use 'model = Book' instead of 'queryset' (read comments for detail)

.. code-block:: python
    :linenos:

    # books/views.py

    from django.http import HttpResponse 
    from django.shortcuts import render
    from django.views.generic import TemplateView, ListView, DetailView

    [...]

    # book details using class based view
    # we can use model=Book in place of queryset
    class BookDetail(DetailView):
        model = Book  # use this or below 
        # queryset = Book.objects.all()



Add following urls for this new view, 

.. code-block:: python
    :linenos:

    # books/urls.py

    from django.conf.urls import url

    from .views import (    http_example,
            fexample, CExample,
            book_list, BookList,
            BookDetail          
        )

    # f in the link name, is used for function based view, otherwise class based view

    urlpatterns = [
        url(r'^httpExample/$', http_example, name="httpExample"), # HttpResponse
        url(r'^fexample/$', fexample, name="fexample"), # function-based-view
        url(r'^cexample/$', CExample.as_view(), name="cexample"), # class-based-view

        
        url(r'^fbooklist/$', book_list, name="fbooklist"), # Book list
        url(r'^$', BookList.as_view(), name="booklist"), # Book list

        url(r'^(?P<pk>\d+)/$', BookDetail.as_view(), name="bookdetail"), # Book list
    ]


Also, modify the 'book_list.html' to add the link for the detail page, 

.. note::

    It is better to use the 'get_absolute_url' method for adding the link as shown in Line 15. (instead of method at Line 14). 

    For this we need to add the method 'get_absolute_url' in the models.py. In this way, if we decide to change the link later then we need to update the models.py only. 

    .. code-block:: python
        :linenos:

        # books/models.py

        from django.db import models
        from django.utils.text import slugify
        from django.db.models.signals import post_save, pre_save
        from django.core.urlresolvers import reverse 

        [...]

        class Book(models.Model):
            title       = models.CharField(max_length=100)
            [...]
                                            )
            def __str__(self):
                return self.title

            # url for the book detail page
            def get_absolute_url(self):
                return reverse("book:bookdetail", kwargs={'pk': self.pk})
                # return reverse("book:bookdetail", args=[str(self.pk)])
                # return reverse("book:bookdetail", args=[str(self.id)])

            # url for booklist page
            def get_book_list_url(self):
                return reverse("book:booklist")


.. code-block:: html
    :linenos: 
    :emphasize-lines: 14-15

    <!-- books/templates/books/books_list.html -->

    {% extends "base.html" %}
    {% load staticfiles %}

    {% block content %}

        <h2> Hello {{ name }} </h2>

        <h4> List of books </h4>

        <ul>
        {% for obj in object_list %}
            <!-- <li> <a href=" {% url 'book:bookdetail' obj.id %}"> {{ obj }} </a> </li> -->
            <li> <a href="{{ obj.get_absolute_url }}"> {{ obj }} </a> </li>
        {% endfor %}
        </ul>

    {% endblock %}


Below is the HTML which is used by the views, 

.. note:: 

    * '**forloopcounter**' starts with 1. 
    * '**forloopcounter0**' starts with 0. 
    * '**object.authors.all (i.e object.fieldName.all**' is used for **many-to-many** field. 
    * '**object.format_set.all (i.e. object.className_set.all)**' is used for **ForeignKey**. 


.. code-block:: html

    <!-- books/templates/books/books_detail.html -->

    {% extends "base.html" %}
    {% load staticfiles %}

    {% block content %}

        <h2> Book details </h2>

        <br>
        <h4> Title : {{ object.title }} </h4>

        <br>
        <h4> Formats and price (ForeignKey) </h4>
        <h6> Total format : {{object.format_set.all.count}} </h6>
        {% for format in object.format_set.all %}
            <!-- forloop.counter  starts from 1 -->
            <p> {{ forloop.counter }}: {{ format }} $ {{format.price}} </p>
        {% endfor %}
       
        <br>
        <h4> Authors (Many-to-many)</h4>
        <h6> Number of authors : {{object.authors.count}} </h6>
        {% for author in object.authors.all %}
            <!-- forloop.counter0  starts from 0 -->
            <p> {{ forloop.counter0 }}:  {{author}} </p>
        {% endfor %}

        <br>
        <!-- <a href="{% url 'book:booklist' %}"><p class="btn btn-info">Back to list</p></a> -->

        <!-- i.e go to 'class-book in current folder' and 'get_book_list_url' -->
        <a href="{{book.get_book_list_url}}"><p class="btn btn-info">Back to list</p></a>
    {% endblock %}


After, these settings we will have the detail page as shown in :numref:`fig_view4`


.. _`fig_view4`: 

.. figure:: fig/view/fig_view4.png

   Detail page


Update all_link.html
====================


For easy viewing of each page, add the following code in the 'all_links.html'. :numref:`fig_view5` shows the home page after updating the 'all_link.html'

.. code-block:: html

    <!-- templates/bookstore/all_links.html -->

    {% extends "base.html" %}
    {% load staticfiles %}

    {% block content %}

        <h1> Links </h1>

        <table class="table">
            <tr>
                <th> View name ( with URL) </th>
                <th> Type </th>
                <th> Description</th>
            </tr>
            <tr>
                <td> <a href="{% url 'home' %}">bookstore/views/all_links</a></td>
                <td> Home page of this website </td>
                <td>List of links</td>
            </tr>
            <tr>
                <td> <a href="{% url 'book:httpExample' %}">books/views/http_example</a></td>
                <td> function based view </td>
                <td>HttpResponse</td>
            </tr>
            <tr>
                <td> <a href="{% url 'book:fexample' %}">books/views/fexample</a></td>
                <td> function based view </td>
                <td>Context in "function based" view</td>
            </tr>
            <tr>
                <td> <a href="{% url 'book:cexample' %}">books/views/cexample</a></td>
                <td> TemplateView </td>
                <td>Context in "Class based" view</td>
            </tr>
            <tr>
                <td> <a href="{% url 'book:fbooklist' %}">books/views/book_list</a></td>
                <td> function based view </td>
                <td>Book list</td>
            </tr>
            <tr>
                <td> <a href="{% url 'book:booklist' %}">books/views/BookList</a></td>
                <td> ListView </td>
                <td>Book list</td>
            </tr>
            <tr>
                <td> <a href="{% url 'book:booklist' %}">books/views/BookDetail</a></td>
                <td> DetailView </td>
                <td>Go to list page and click on any book for details </td>
            </tr>
        </table>

    {% endblock %}


.. _`fig_view5`: 

.. figure:: fig/view/fig_view5.png      

   Home page


.. _`sec_read_para_url`: 

Read parameter from URL
=======================

In this section, we will learn to read the parameter from the URL.  

Add below code at the end of the file, 


.. code-block:: python
    :linenos:

    # books/views.py

    [...]

    # read args using function-based view
    def read_arg(request, val):
        template_name = 'books/read_url.html'
        context = {
            "value" : val
        }
        return render(request, template_name, context)

    # read kwargs using function-based view
    def read_kwarg(request, test):  # test is defined in 'url' (cant be changed)
        template_name = 'books/read_url.html'
        context = {
            "value" : test
        }
        return render(request, template_name, context)

    # read args using class-based view
    class ReadArg(TemplateView):
        template_name = 'books/read_url.html'
        def get_context_data(self, *args, **kwargs):
            context = super().get_context_data(*args, **kwargs)
            context["value"] = self.args[0]
            return context
     
    # read kwargs using class-based view
    class ReadKwarg(TemplateView):
        template_name = 'books/read_url.html'
        def get_context_data(self, *args, **kwargs):
            context = super().get_context_data(*args, **kwargs)
            # test is defined in 'url' (cant be changed)
            context["value"] = (self.kwargs.get("test"))
            return context


Update urls.py as below, 

.. note:: 

    The parameter inside the brackets '( )' can be read in the view e.g. (\\w+) and (?P<test>\\w+)/ are readable parameters. Also '?P<test>' will pass the parameter as keyword arguments. 

.. code-block:: python
    :linenos: 
    
    # books/urls.py

    from django.conf.urls import url

    from .views import (    http_example,
            fexample, CExample,
            book_list, BookList,
            BookDetail,
            read_arg, read_kwarg, ReadArg, ReadKwarg,
        )

    # f in the link name, is used for function based view, otherwise class based view

    urlpatterns = [
        [...]

        url(r'^freadargs/(\w+)/$', read_arg, name="freadargs"), # Read from url
        url(r'^readargs/(\w+)/$', ReadArg.as_view(), 
                                        name="readargs"), # Read from url
        url(r'^freadkwargs/(?P<test>\w+)/$', read_kwarg, 
                                        name="freadkwargs"), # Read from url
        url(r'^readkwargs/(?P<test>\w+)/$', ReadKwarg.as_view(), 
                                        name="readkwargs"), # Read from url

    ]


Now, see the following urls, and all the url will show the results shown in :numref:`fig_view6`

.. code-block:: text

    http://127.0.0.1:8000/books/freadargs/tiger/
    http://127.0.0.1:8000/books/readargs/tiger/
    http://127.0.0.1:8000/books/freadkwargs/tiger/
    http://127.0.0.1:8000/books/readkwargs/tiger/

.. _`fig_view6`: 

.. figure:: fig/view/fig_view6.png      

   Read value from the URL


In the below code, see the methods by which we can pass the 'args' and 'kwargs' in the 'href', 

.. code-block:: html

    <!-- templates/bookstore/all_links.html -->

    {% extends "base.html" %}
    {% load staticfiles %}

    {% block content %}

        <h1> Links </h1>

        <table class="table">
            [...]

            <tr>
                <td> <a href="{% url 'book:freadargs' 'tiger' %}">books/views/read_arg</a></td>
                <td> function based view </td>
                <td> Read args from URL </td>
            </tr>
            <tr>
                <td> <a href="{% url 'book:readargs' 'tiger' %}">books/views/ReadArgs</a></td>
                <td> TemplateView </td>
                <td> Read args from URL <br> 
                    <b>url 'book:readargs' 'tiger'</b></td>
            </tr>
            <tr>
                <td> <a href="{% url 'book:freadkwargs' test='tiger' %}">books/views/read_kwarg</a></td>
                <td>  function based view  </td>
                <td> Read kwargs from URL <br>
                     <b>url 'book:freadkwargs' test='tiger'</b> </td>
            </tr>
            <tr>
                <td> <a href="{% url 'book:readkwargs' test='tiger' %}">books/views/ReadKwarg</a></td>
                <td> TemplateView </td>
                <td> Read kwargs from URL </td>
            </tr>
        </table>

    {% endblock %}



Conclusion
==========

In this chapter, we saw the 'function-based views' and 'class-based views'. Also, we saw the method by which the 'readable' arguments can be passed into the URL. These arguments were read by the 'views' and printed on the HTML page. In the next chapter will learn to get value in the "views" from the "forms" along with search operations. 