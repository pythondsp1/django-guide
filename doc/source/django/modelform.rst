Model Forms
***********

.. note:: 

   Download :download:`registration-html-files with crispy-tags <zip_codes/registration.tar.gz>` for easy registration which is discussed in :numref:`sec_crispy_form_t` and :numref:`sec_django_regis_redux_x`. Copy the extracted folder 'registration' inside the 'templates' folder in the root-folder (i.e. **templates/registration/html-files**). 


   In some of the listings, only the newly added codes are shown. Therefore the complete codes of this chapter is provided which can be  :download:`downloaded from here <zip_codes/codes-6.tar.gz>`. This will be useful if we have problems in understanding the 'partial part' of the code.  


Introduction
============

In this chapter, we will create  various forms. 


ModelForm : Create author form (function based views)
=====================================================


We already create a form in :numref:`py_form_py_cr` for Authors in the Admin page. Let's use that form to create the form using 'views'. 

.. note::

    * If we want to process the form then we should use 'form.save(commit=False)' and then modify the things; and finally save the form. 

    * If we do not anything to process then we can use 'form.save()' directly after validating the form. 

.. code-block:: python

    # books/views.py

    from django.db.models import Q
    from django.http import HttpResponse 
    from django.shortcuts import render, redirect
    from django.views.generic import TemplateView, ListView, DetailView

    from .forms import AuthorForm
    from .models import Book

    [...]

    def author_create(request):
        form = AuthorForm(request.POST or None)
        error = None
        if form.is_valid():
            # # use below to save
            # form.save() 
            # #  or below if want to perform some operations before saving
            new_item = form.save(commit=False)
            context ={
                "item" : "Author", 
                "title" : new_item.first_name
            }
            new_item.save()
            template_name = 'books/thanks_create.html'
            return render(request, template_name, context)
        else:
            errors = form.errors
            template_name = 'books/create_form.html'
            context = {
                    "form" : form, 
                    "errors" : errors, 
                    "item" : "Author"
                }
            return render(request, template_name, context)


* Now add the form link in the urls.py file, 

.. code-block:: python

    # books/urls.py

    from django.conf.urls import url

    from .views import (    http_example,
            fexample, CExample,
            book_list, BookList,
            BookDetail,
            read_arg, read_kwarg, ReadArg, ReadKwarg,
            SearchBooks, QSearchBooks, BookSearch, BookSearchResult, 
            author_create,
        )

    # f in the link name, is used for function based view, otherwise class based view

    urlpatterns = [
        [...]

        url(r'^fauthorcreate/$', author_create, 
                                        name="fauthorcreate"), # create author
    ]


* Next, create the html page for the form; the 'form.as_p' is the quickest way to create the form, but it may not have the very good look. 

.. code-block:: html

    <!-- books/templates/books/create_form.html -->

    {% extends "base.html" %}
    {% load staticfiles %}

    {% block content %}

        <h1> Add {{ item }} </h1>

        {% if errors %}
            <span style="color: red">{{ errors }}</span>
        {% endif %}

        <form method="post"> {% csrf_token %}
            {{ form.as_p }}
            <button type="submit" class="btn btn-info"> Save </button>
        </form>
    {% endblock %}



* Finally create the 'Thanks' page for successfully created page. 

.. code-block:: html

    <!-- books/templates/books/thanks_create.html -->

    {% extends "base.html" %}
    {% load staticfiles %}

    {% block content %}

        <h4> Thank you </h4>
        <p> {{item}} "{{title}}" is created successfully</p>

        <a href="/"><button class="btn btn-info">Home Page</button></a>

    {% endblock %}

* Now go to link 'http://127.0.0.1:8000/books/fauthorcreate/' to create an Author as shown in :numref:`fig_form1`


.. _`fig_form1`:

.. figure:: fig/form/fig_form1.png

   Create author


.. note:: 

    Currently we do not have any view to see the list of Authors, therefore go to admin page to see the newly added Author. 


Or create a Author-list page as below, 

.. code-block:: python

    # books/views.py

    [...]

    from .models import Book, Author
    [...]

    class AuthorList(ListView): 
        # # default template location : lowercaseAppName/lowercaseAppName_list.html
        # template_name = "books/book_list.html"

        # by default 'queryset' is sent as context 
        queryset = Author.objects.all()  # 'queryset' is reserved name like 'template_name'

        # to pass more context, inherit cotext with 'super' and 
        # then include the items in the context  using 'get_context_data'
        def get_context_data(self, *args, **kwargs): 
            context = super().get_context_data(*args, **kwargs)
            context["name"] = "Harry"
            return context


.. code-block:: python

    # books/urls.py

    from django.conf.urls import url

    from .views import (    http_example,
            fexample, CExample,
            book_list, BookList, AuthorList,
            [...]
        )

    urlpatterns = [
        [...]
        url(r'^authorlist/$', AuthorList.as_view(), name="authorlist"), # Author list
        [...]
    ]


.. code-block:: html

    <!-- books/templates/books/books_list.html -->

    {% extends "base.html" %}
    {% load staticfiles %}

    {% block content %}

        <h2> Hello {{ name }} </h2>

        <h4> List of authors </h4>

        <ul>
        {% for obj in object_list %}
            <li> {{ obj }} </li>
        {% endfor %}
        </ul>

    {% endblock %}


* Now we can see the list of authors at http://127.0.0.1:8000/books/authorlist/


.. _`sec_crispy_form_t`: 

Crispy form (third party app)
=============================

The look of the form in :numref:`fig_form1` is not good. We can improve it by writing HTML code for the form; or by using the third party app 'Crispy form' which can be install as below. 

.. code-block:: shell

    $ pip install django-crispy-forms

.. code-block:: python

    # settings.py 

    INSTALLED_APPS = [
        'django.contrib.admin',
        [...]

        # third parth apps

        #crispy-forms
        'crispy_forms',
        
        
        # my app
        'books', 
    ]


    # add at the end
    #crispy form settings
    CRISPY_TEMPLATE_PACK = 'bootstrap3'


* Next modify the create_form.html file as below, 

.. code-block:: html
    :linenos: 
    :emphasize-lines: 5, 18-19, 9-10, 22-23

    <!-- books/templates/books/create_form.html -->

    {% extends "base.html" %}
    {% load staticfiles %}
    {% load crispy_forms_tags %}

    {% block content %}

        <div class="row">
        <div class="col-sm-offset-3 col-sm-4">
            <h1> Add {{ item }} </h1>

            {% if errors %}
                <span style="color: red">{{ errors }}</span>
            {% endif %}

            <form method="post"> {% csrf_token %}
                <!-- {{ form.as_p }} -->
                {{ form|crispy}}
                <button type="submit" class="btn btn-info"> Save </button>
            </form>
        </div>
        </div>
    {% endblock %}


* Or we can use below code for custom style, which is still not as good as above code, 

.. code-block:: html

    <!-- books/templates/books/create_form.html -->

    {% extends "base.html" %}
    {% load staticfiles %}
    {% load crispy_forms_tags %}

    {% block content %}

        <div class="row">
        <div class="col-sm-offset-3 col-sm-4">
            <h1> Add {{ item }} </h1>

            {% if errors %}
                <span style="color: red">{{ errors }}</span>
            {% endif %}

            <form role="form" method="post"> {% csrf_token %}
                <div  class="form-group">
                    <input class="form-control"  type="text" name="first_name" placeholder="first name"><br>
                    <input class="form-control" type="text" name="middle_name" placeholder="middle name"><br>
                    <input class="form-control" type="text" name="last_name" placeholder="last name"><br>
                    <input class="form-control" type="text" name="email" placeholder="email name">
                </div>

                <div > <b>Age</b><br>
                        <input type="radio" name="age"  value="0" checked> < 13 <br>
                        <input type="radio" name="age" value="1">  13-19 <br>
                        <input type="radio" name="age" value="2"> > 19 <br>
                </div>

                <br>
                <button type="submit" class="btn btn-info"> Save </button>
            </form>
        </div>
        </div>
    {% endblock %}



Login required to add data (function based view)
================================================


.. _`html_url_settings_ad`:

HTML and URL settings
---------------------

* First add the following urls in the 'settings.py'

.. code-block:: python
    
    # settings.py 

    LOGIN_URL = '/login/'
    LOGIN_REDIRECT_URL = '/'
    LOGOUT_REDIRECT_URL = '/login/'


* Next add these urls in the urls.py,

.. code-block:: python
    :linenos: 
    :emphasize-lines: 18-20

    # Configuration-folder/urls.py

    from django.conf.urls import url, include
    from django.conf.urls.static import static
    from django.contrib import admin
    from django.conf import settings
    from django.contrib.auth.views import LoginView, LogoutView

    from .views import all_links 


    urlpatterns = [
        url(r'^admin/', admin.site.urls),
        url(r'^$', all_links, name="home"),  # home page

        url(r'^books/', include("books.urls", namespace="book")), # books app

        # default location for html is 'registration/login.html'
        url(r'^login/', LoginView.as_view(), name='login' ), 
        url(r'^logout/', LogoutView.as_view(), name='logout' ), 
    ]


    # required to see the media files in debug mode
    if settings.DEBUG:
        urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


* Finally create the 'login.html'. Note that 'LoginView' is used in the 'urls.py' which looks for 'login.html' file inside the 'registration' folder. Hence create a folder 'registration' inside the folder 'templates' and save 'login.html' there. 

.. code-block:: html

    <!-- templates/registration/login.html -->

    {% extends "base.html" %}
    {% load staticfiles %}
    {% load crispy_forms_tags %}

    {% block content %}
    <div class='col-md-6 col-md-offset-2'>
        <h3>Please Login</h3><br>
        <form method="post" action="">
            {% csrf_token %}
            {{ form|crispy}}
            <input class='btn btn-primary' type="submit" value="login" />
        </form>
    </div>
    {% endblock %}


Only Login required
-------------------

The ' \@login_required() ' decorator is enough to allow only registered user to add the data. Note that we used the code in :numref:`sec_staff_login_required` to create a login-required view, as that allows only 'staffs' to add data. 

.. code-block:: python

    # books/views.py

    from django.db.models import Q
    from django.http import HttpResponse 
    from django.shortcuts import render, redirect
    from django.views.generic import TemplateView, ListView, DetailView
    from django.contrib.auth.decorators import login_required

    from .forms import AuthorForm
    from .models import Book

    [...]

    @login_required() # LOGIN_URL = '/login/' in settings.py
    # '/login/' is the url-name' saved in urls.py in configuration-folder
    # or use below, 
    # @ login_required(login_url='/login/')
    def author_create(request):
        form = AuthorForm(request.POST or None)
        [...]


.. _`sec_staff_login_required`:

Staff login required
--------------------

In the previous case, any registered user can add data in the database; but we can restrict it to 'staff' only. The code is slightly modified, so that only staff can add the data. Now if any 'registered user without staff status' tries to add the data then 'error' message will be displayed as shown in :numref:`fig_form2`. 

.. code-block:: python

    # books/views.py

    from django.db.models import Q
    from django.http import HttpResponse 
    from django.shortcuts import render, redirect
    from django.views.generic import TemplateView, ListView, DetailView
    from django.contrib.auth.decorators import login_required

    from .forms import AuthorForm
    from .models import Book

    [...]

    @login_required() # LOGIN_URL = '/login/' in settings.py
    # '/login/' is the url-name' saved in urls.py in configuration-folder
    # or use below, 
    # @ login_required(login_url='/login/')
    def author_create(request):
        form = AuthorForm(request.POST or None)
        error = None
        if form.is_valid() and request.user.is_staff:
            # # use below to save
            # form.save() 
            # #  or below if want to perform some operations before saving
            new_item = form.save(commit=False)
            # if 
            context ={
                "item" : "Author", 
                "title" : new_item.first_name
            }
            new_item.save()
            template_name = 'books/thanks_create.html'
            return render(request, template_name, context)
        else:
            if not request.user.is_staff:
                errors = "Only staff can add data!"
            else:
                errors = form.errors
            template_name = 'books/create_form.html'
            context = {
                    "form" : form, 
                    "errors" : errors, 
                    "item" : "Author"
                }
            return render(request, template_name, context)


.. _`fig_form2`:

.. figure:: fig/form/fig_form2.png

   Staff login required to add data


.. note:: 

    Further, we can user 'permission_required' decorator as well. Only problem is that it will not display the message "Only staff can add data!"

    .. code-block:: python
    
        # books/views.py

        from django.db.models import Q
        from django.http import HttpResponse 
        from django.shortcuts import render, redirect
        from django.views.generic import TemplateView, ListView, DetailView
        from django.contrib.auth.decorators import login_required, permission_required

        from .forms import AuthorForm
        from .models import Book

        [...]

        @permission_required('user.is_staff')
        def author_create(request):
            form = AuthorForm(request.POST or None)
            error = None
            if form.is_valid():
                # # use below to save
                # form.save() 
                # #  or below if want to perform some operations before saving
                new_item = form.save(commit=False)
                context ={
                    "item" : "Author", 
                    "title" : new_item.first_name
                }
                new_item.save()
                template_name = 'books/thanks_create.html'
                return render(request, template_name, context)
            else:
                errors = form.errors
                template_name = 'books/create_form.html'
                context = {
                        "form" : form, 
                        "errors" : errors, 
                        "item" : "Author"
                    }
                return render(request, template_name, context)




.. _`sec_django_regis_redux_x`:

Django registration redux (Third party app)
===========================================

.. note:: 

    There are few small settings in this part. Please :download:`download the code <zip_codes/codes-5.tar.gz>` if there is any problem with this part. 


Django-registration-redux is the third party app, which can be used for **creating registration** forms easily. This has following features, 

* Login 
* Logout
* Regiseter with activate through mail
* Password change
* Password reset through email

Below are the links for above facilities, 

.. code-block:: text
    
    ^accounts/ ^activate/complete/$ [name='registration_activation_complete']
    ^accounts/ ^activate/resend/$ [name='registration_resend_activation']
    ^accounts/ ^activate/(?P<activation_key>\w+)/$ [name='registration_activate']
    ^accounts/ ^register/complete/$ [name='registration_complete']
    ^accounts/ ^register/closed/$ [name='registration_disallowed']
    ^accounts/ ^register/$ [name='registration_register']
    ^accounts/ ^login/$ [name='auth_login']
    ^accounts/ ^logout/$ [name='auth_logout']
    ^accounts/ ^password/change/$ [name='auth_password_change']
    ^accounts/ ^password/change/done/$ [name='auth_password_change_done']
    ^accounts/ ^password/reset/$ [name='auth_password_reset']
    ^accounts/ ^password/reset/complete/$ [name='auth_password_reset_complete']
    ^accounts/ ^password/reset/done/$ [name='auth_password_reset_done']
    ^accounts/ ^password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$ [name='auth_password_reset_confirm'] 


* It can be installed as below, 

.. code-block:: python

    pip install django-registration-redux


.. note:: 

   * Delete the 'login.html' file from the 'templates/registration' folder which we created in :numref:`html_url_settings_ad`. 

   * Download :download:`registration-html-files with crispy-tags <zip_codes/registration.tar.gz>`. In these files, 'crispy tags' are added in the 'django-crispy-forms' files. Save the extracted folder 'registration' inside the folder 'templates'  which is in the 'rood-folder' (i.e. **templates/registration/html-files**)




settings.py
-----------

* After installing, we need to add it to the Installed_app as below. 

.. note:: 
    
    We have commented the following lines as it is already in the django-registration-redux forms, 

    # LOGIN_URL = '/login/'
    # LOGIN_REDIRECT_URL = '/'
    # LOGOUT_REDIRECT_URL = '/login/'


    Also, see the 'django-registration-redux settings' which is required for registration through emails. 


.. code-block:: python

    # Application definition

    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',

        # third parth apps

        #crispy-forms
        'crispy_forms',
        # django-registration-redux
        'registration',
        
        # my app
        'books', 
    ]

    [...]
    # LOGIN_URL = '/login/'
    # LOGIN_REDIRECT_URL = '/'
    # LOGOUT_REDIRECT_URL = '/login/'

    #crispy form settings
    CRISPY_TEMPLATE_PACK = 'bootstrap3'

    #django-registration-redux settings
    ACCOUNT_ACTIVATION_DAYS=7
    REGISTRATION_AUTO_LOGIN=True
    SITE_ID=1
    LOGIN_REDIRECT_URL='/'

* After this, add below lines to both  **_development_settings.py** and **_production_settings.py**. This is for different email-settings form 'development' and 'production' server; otherwise we can put it in the 'settings.py'. 

.. note:: 

    * Below is the email settings for the 'gmail', which need to be changed for other providers. 
    * Also, even if the settings are wrong, the registered-user will be added in the database; and we can manually activate the user through admin page. 
  
.. code-block:: python

    # Email
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_HOST_USER = os.environ.get("EMAIL_HOST_USER")
    EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_PASSWORD')
    EMAIL_PORT = 587
    EMAIL_USE_TLS = True


urls.py
-------

Next add the Line 22 in the urls.py, so that all the links will be available as 'accounts/...'. We can change the word 'accounts' with any other words. Lastly comment the Lines 18-20 as we do not need it because the Line 22 includes both 'login' and 'logout' facilities. 

.. note:: 

    If we want to change the word 'account', then change the 'navbar.html' accordingly. It is better to not to touch it. 

.. code-block:: python
    :linenos: 
    :emphasize-lines: 18-20, 22

    # Configuration-folder/urls.py

    from django.conf.urls import url, include
    from django.conf.urls.static import static
    from django.contrib import admin
    from django.conf import settings
    from django.contrib.auth.views import LoginView, LogoutView

    from .views import all_links 


    urlpatterns = [
        url(r'^admin/', admin.site.urls),
        url(r'^$', all_links, name="home"),  # home page

        url(r'^books/', include("books.urls", namespace="book")), # books app

        # default location for html is 'registration/login.html'
        # url(r'^login/', LoginView.as_view(), name='login' ), 
        # url(r'^logout/', LogoutView.as_view(), name='logout' ), 

        url(r'^accounts/', include('registration.backends.default.urls')),
    ]


    # required to see the media files in debug mode
    if settings.DEBUG:
        urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


navbar.html
-----------

Finally, add the login/logout/register in the navbar. If user is logged-in then 'logout' and 'reset password' will be shown in the navbar, otherwise 'register' and 'login' will be displayed. 

.. code-block:: html

    <!-- templates/navbar.html  -->
    <!-- if you are using this template for other project,  -->
    <!-- then first remove the django-urls in the below code (if any)  -->
    <!-- e.g. replace {% url "book:booksearchresult" %} with # -->


    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="nav navbar-brand" href="/">Bookstore</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="/">Home</a></li>
            <!-- <li><a href="/books">All Books</a></li> -->

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header">Nav header</li>
                  <li><a href="#">Separated link</a></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
            </li>
          </ul>

          <form class="navbar-form pull-left" method="GET" action='/books/booksearchresult/'>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Search Books" name="q">
              </div>
          </form>

          <ul class="nav navbar-nav navbar-right">
            {% if request.user.is_authenticated %}
                <li><a href="/accounts/password/change">Reset Password</a></li>
                <li><a href="/accounts/logout">Logout</a></li>
            {%else%}
                <li><a href="/accounts/login">Login</a></li>
                <li><a href="/accounts/register">Register</a></li>
            {%endif%}
          </ul>

        </div><!--/.nav-collapse 
        -->
      </div>
    </nav>



ModelForm : Create author form (class based views)
==================================================


The same can be achieved using 'class based views' as well, 


* Any one can create the form (not used in the code), 

.. code-block:: python
    
    # books/views.py

    from django.db.models import Q
    from django.http import HttpResponse 
    from django.shortcuts import render, redirect
    from django.views.generic import TemplateView, ListView, DetailView, CreateView
    from django.contrib.auth.decorators import login_required, permission_required
    from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
    [...]

    class AuthorCreate(CreateView):
        form_class = AuthorForm
        template_name = 'books/create_form.html'
        success_url = "/books/authorlist/"

        def form_valid(self, form):
            new_item = form.save(commit=False)
            # do something here
            return super().form_valid(form)


* Login required to add the data (not used in the code), 

.. code-block:: python

    # books/views.py

    from django.db.models import Q
    from django.http import HttpResponse 
    from django.shortcuts import render, redirect
    from django.views.generic import TemplateView, ListView, DetailView, CreateView
    from django.contrib.auth.decorators import login_required, permission_required
    from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
    [...]

    class AuthorCreate(LoginRequiredMixin, CreateView):
        form_class = AuthorForm
        template_name = 'books/create_form.html'
        success_url = "/books/"

        def form_valid(self, form):
            new_item = form.save(commit=False)
            # do something here
            return super().form_valid(form)


* Only staff can create the data (this is used in code), 

.. code-block:: python

    # books/views.py

    from django.db.models import Q
    from django.http import HttpResponse 
    from django.shortcuts import render, redirect
    from django.views.generic import TemplateView, ListView, DetailView, CreateView
    from django.contrib.auth.decorators import login_required, permission_required
    from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
    [...] 

    class AuthorCreate(PermissionRequiredMixin, CreateView):
        permission_required = 'user.is_staff'
        form_class = AuthorForm
        template_name = 'books/create_form.html'
        success_url = "/books/authorlist/"

        def form_valid(self, form):
            new_item = form.save(commit=False)
            # do something here
            return super().form_valid(form)

        def get_context_data(self, *args, **kwargs): # heading for Form
            context = super().get_context_data(*args, **kwargs)
            context["item"] = "Author"
            return context


* Add the above view in the URL, 

.. code-block:: python

    # books/urls.py

    from django.conf.urls import url

    from .views import (    http_example,
            fexample, CExample,
            book_list, BookList,
            BookDetail,
            read_arg, read_kwarg, ReadArg, ReadKwarg,
            SearchBooks, QSearchBooks, BookSearch, BookSearchResult, 
            author_create, AuthorCreate,
        )

    # f in the link name, is used for function based view, otherwise class based view

    urlpatterns = [
        [...]

        url(r'^authorcreate/$', AuthorCreate.as_view(), 
                                        name="authorcreate"), # create author
    ]


Validations
===========

.. _`sec_valid1_rule`:

Validation through form
-----------------------

We can perform validation using 'clean' function in 'forms.py' as shown below. The below code does not allow "Hello" as the first name as shown in :numref:`fig_form3`

.. note:: 

    The function name for the validation should be 'clean_fieldName'.

.. code-block:: python

    # books/forms.py

    from django import forms
    from django.contrib import admin

    from .models import Author


    class AuthorForm(forms.ModelForm):
        class Meta:
            fields = ['first_name', 'middle_name', 'last_name', 'email', 'age']
            model = Author

        # clean_NameOfField  # validation
        def clean_first_name(self):
            name = self.cleaned_data.get("first_name")
            print(name)
            if name.upper() == "HELLO":
                raise forms.ValidationError("Not a valid name")
            return name

.. _`fig_form3`:

.. figure:: fig/form/fig_form3.png

   Form validation through forms.py


Validation using validators.py
------------------------------

It is better to save the all the validation rules in a file and then use it in the **forms** and **models** as shown in this section. 

validators.py
^^^^^^^^^^^^^

First create a file 'validators.py' and save the rules in it as shown below, 


.. code-block:: python
    :linenos: 

    # books/validators.py

    from django.core.exceptions import ValidationError

    def valid_email(val):
        if ".cmo" in val:
            raise ValidationError(".com (not '.cmo') ")


    LAST_NAME = ["TIGER", "CAT", "PYTHON"]
    def validate_last_name(val):
        if val.upper() in LAST_NAME:
            err = "%s is not allowed as last name" % (val) 
            raise ValidationError(err)


.. _`sec_valid2_rule`:

Add rule to forms
^^^^^^^^^^^^^^^^^

Next add rule in the form as shown in Line 10. If we put 'last name' as 'tiger' then it will not be allowed as shown in :numref:`fig_form4`. 

.. code-block:: python
    :linenos: 
    :emphasize-lines: 10

    # books/forms.py

    from django import forms
    from django.contrib import admin

    from .models import Author
    from .validators import validate_last_name

    class AuthorForm(forms.ModelForm):
        last_name = forms.CharField(validators=[validate_last_name])
        class Meta:
            fields = ['first_name', 'middle_name', 'last_name', 'email', 'age']
            model = Author

        # clean_NameOfField
        def clean_first_name(self): # validation
            name = self.cleaned_data.get("first_name")
            print(name)
            if name.upper() == "HELLO":
                raise forms.ValidationError("Not a valid name")
            return name

.. _`fig_form4`:

.. figure:: fig/form/fig_form4.png

   Form validation through validators.py and forms.py

.. _`sec_valid3_rule`:

Add rule in model.py
--------------------

While defining the fields, we can add the rule directly in the models.py as well, as shown below, 

.. code-block:: python
    :linenos: 
    :emphasize-lines: 7, 28

    # books/models.py

    from django.db import models
    from django.utils.text import slugify
    from django.db.models.signals import post_save, pre_save
    from django.core.urlresolvers import reverse 
    from .validators import valid_email

    AGE_GROUP = (
        (0, "< 13"), 
        (1, "13-19"),
        (2, "> 19")
    )
        
    BOOK_FORMAT = (
        (0, "PDF"), 
        (1, "Hard Cover"),
        (2, "Epub"), 
        (3, "HTML")
    )


    class Author(models.Model):
        first_name = models.CharField(max_length=50)
        middle_name = models.CharField(max_length=50, blank=True, null=True)
        last_name = models.CharField(max_length=50)
        age = models.IntegerField(choices=AGE_GROUP, default=2) # > 19
        email = models.EmailField(validators=[valid_email])
        [...]

Now run the migration commands, 

.. code-block:: text

    $ python manage.py makemigrations

    $ python manage.py migrate


Now, try to type '.cmo' in the form and an error will be shown as displayed in :numref:`fig_form5`


.. _`fig_form5`:

.. figure:: fig/form/fig_form5.png

   Form validation through validators.py and models.py


.. note:: 

    In this section, we saw three methods for validating the form data, which are shown in :numref:`sec_valid1_rule`, :numref:`sec_valid2_rule` and :numref:`sec_valid3_rule`. 


Update data (only staff or author can update)
=============================================

In this section, we will see the UpdateView to edit the existing data. 

* First create a form for model Book in forms.py, 

.. code-block:: python
    :linenos: 

    # books/forms.py

    from django import forms
    from django.contrib import admin

    from .models import Author, Book
    from .validators import validate_last_name

    [...]

    class BookForm(forms.ModelForm):
        class Meta:
            model = Book
            # fields = '__all__'
            exclude = ['slug'] # do not include slug in form


* Below code allows staff to edit all books; whereas register user can edit his own book only if the 'first name' and 'username' are same, 

.. code-block:: python
    :linenos: 

    # books/views.py

    from django.db.models import Q
    from django.http import HttpResponse 
    from django.shortcuts import render, redirect
    from django.views.generic import (TemplateView, ListView, DetailView, 
        CreateView, UpdateView)
    from django.contrib.auth.decorators import login_required, permission_required
    from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

    from .forms import AuthorForm, BookForm
    from .models import Book, Author

    [...] 

    class BookUpdate(LoginRequiredMixin, UpdateView):
        template_name = "books/book_update.html"
        form_class = BookForm

        def get_queryset(self): # if staff allow all book updates
            user = self.request.user
            if user.is_staff:
                queryset = Book.objects.all()
            else: # filter books with username = Author first name
                queryset = Book.objects.filter(authors__first_name__icontains=user)
            return queryset


* Next add the view in urls.py

.. code-block:: python
    :linenos: 

    # books/urls.py

    from django.conf.urls import url

    from .views import (    http_example,
            fexample, CExample,
            book_list, BookList, AuthorList,
            BookDetail,
            read_arg, read_kwarg, ReadArg, ReadKwarg,
            SearchBooks, QSearchBooks, BookSearch, BookSearchResult, 
            author_create, AuthorCreate, BookUpdate,
        )

    # f in the link name, is used for function based view, otherwise class based view

    urlpatterns = [
        [...]

        url(r'^(?P<pk>\d+)/update/$', BookUpdate.as_view(), 
                                        name="bookupdate"), # update book
    ]

* Finally create the html for book-update, 

.. code-block:: html
    :linenos: 
    :emphasize-lines: 13

    <!-- books/templates/books/books_update.html -->

    {% extends "base.html" %}
    {% load staticfiles %}
    {% load crispy_forms_tags %}

    {% block content %}

        <div class="row">
        <div class="col-sm-offset-3 col-sm-4">
        <h2> Book Update </h2>

            <h3> {{form.instance.title}} </h3>

            {% if errors %}
                <span style="color: red">{{ errors }}</span>
            {% endif %}

            <form method="post"> {% csrf_token %}
                <!-- {{ form.as_p }} -->
                {{ form|crispy}}
                <button type="submit" class="btn btn-info"> Save </button>
            </form>
        </div>
        </div>

        <a href="{{book.get_book_list_url}}"><p class="btn btn-info">Back to list</p></a>
    {% endblock %}


* Now create a user with user='firname of author of any book'. For example, we create the username "Rower" which is author of only one book. Hence, he can modify his one book only.


.. note:: 

    If 'logged-in user' is not the 'author of the book' then 'Page not found (404)' error  will be generated i.e. second link will generate the 404 error. 

    .. code-block:: text

        http://127.0.0.1:8000/books/4/update/  (can be updated by Rower)

        http://127.0.0.1:8000/books/1/update/  (can not be updated by Rower)

* Further, if we logged-in as staff then we can update all the books. 
  
Conclusion
==========

In this chapter, we create different 'ModelForms' along with CreateView and UpdateView. We saw several authorization and validation methods in the form. In the next chapter, we will learn to deploy the project. 