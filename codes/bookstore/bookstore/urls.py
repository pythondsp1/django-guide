# Configuration-folder/urls.py

from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from django.contrib.auth.views import LoginView, LogoutView

from .views import all_links 


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', all_links, name="home"),  # home page

    url(r'^books/', include("books.urls", namespace="book")), # books app

    # default location for html is 'registration/login.html'
    # url(r'^login/', LoginView.as_view(), name='login' ), 
    # url(r'^logout/', LogoutView.as_view(), name='logout' ), 

    url(r'^accounts/', include('registration.backends.default.urls')),
]


# required to see the media files in debug mode
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
