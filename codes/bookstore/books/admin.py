# books/admin.py

from django.contrib import admin
from django.utils import timezone

# Register your models here.
from .forms import AuthorForm
from .models import Author, Book, BookImage, Format, Publisher

def draft_status(modeladmin, request, queryset):
    queryset.update(
        draft=False,
        publication_date=timezone.now()
    )
draft_status.short_description = 'Mark book as published now' # at admin page


class AuthorAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'email']
    # sort in ascending order
    ordering = ['last_name', 'email'] # first by last_name then by email
    list_filter = ['last_name']
    form = AuthorForm
    save_as = True   # enable save as option
    save_on_top = True  # show the save-buttons on top and bottom
    radio_fields = {'age': admin.HORIZONTAL} # admin.VERTICAL


class BookImageInline(admin.TabularInline):
    model = BookImage
    extra = 2 # optional : show 2 items (default = 3)


class BookInline(admin.StackedInline):
    model = Book
    extra = 1  # show only one item

@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    # note that 'many-to-many (i.e. authors)' field can not be displayed
    list_display = ['title', 'publisher', 'publication_date',
                                'draft', 'updated']
    list_editable = ['draft']
    search_fields = ['title', 'publisher__country', 'authors__last_name']
    actions = [draft_status]
    date_hierarchy = 'publication_date'
    filter_horizontal = ['authors']
    prepopulated_fields = {'slug': ('title',)}

    # both 'fields' and 'fieldsets' can not be specified together
    fieldsets = (
            (None, {  # label 1: None
                'fields': ( # dictionary
                    ('title', 'slug'),
                )
            }),
            ('More details', { # under label 2 : More details
                'classes': ('collapse',),  # css-class : minimized
                'fields': (
                    ('authors'),
                    ('publisher'),
                    'content',
                    ('draft', 'publication_date'),
                )
            })
        )
    inlines = [BookImageInline]


@admin.register(Publisher)
class PublisherAdmin(admin.ModelAdmin):
    list_display = ['admin_name']  # defined in models.py
    # both 'fields' and 'fieldsets' can not be specified together
    inlines = [BookInline]


admin.site.register(Author, AuthorAdmin)
# admin.site.register(Book)
admin.site.register(BookImage)
# admin.site.register(Publisher)

@admin.register(Format)
class FormatAdmin(admin.ModelAdmin):
    save_as = True   # enable save as option
    save_on_top = True  # show the save-buttons on top and bottom
    radio_fields = {'book_format': admin.HORIZONTAL} # admin.VERTICAL
