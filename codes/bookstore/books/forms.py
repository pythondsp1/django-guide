# books/forms.py

from django import forms
from django.contrib import admin

from .models import Author, Book
from .validators import validate_last_name

class AuthorForm(forms.ModelForm):
    last_name = forms.CharField(validators=[validate_last_name])
    class Meta:
        fields = ['first_name', 'middle_name', 'last_name', 'email', 'age']
        model = Author

    # clean_NameOfField
    def clean_first_name(self): # validation
        name = self.cleaned_data.get("first_name")
        print(name)
        if name.upper() == "HELLO":
            raise forms.ValidationError("Not a valid name")
        return name

class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        # fields = '__all__'
        exclude = ['slug'] # do not include slug in form
