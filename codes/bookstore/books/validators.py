# books/validators.py

from django.core.exceptions import ValidationError

def valid_email(val):
    if ".cmo" in val:
        raise ValidationError(".com (not '.cmo') ")


LAST_NAME = ["TIGER", "CAT", "PYTHON"]
def validate_last_name(val):
    if val.upper() in LAST_NAME:
        err = "%s is not allowed as last name" % (val) 
        raise ValidationError(err)