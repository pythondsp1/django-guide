# books/views.py

from django.db.models import Q
from django.http import HttpResponse 
from django.shortcuts import render, redirect
from django.views.generic import (TemplateView, ListView, DetailView, 
    CreateView, UpdateView)
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from .forms import AuthorForm, BookForm
from .models import Book, Author


# Sending HTML code to browser directly 
def http_example(request):
    return HttpResponse("<h3> This is HttpResponse </h3> <p> Thank You! </p>")


# context in function-based views
def fexample(request):
    template_name = 'books/context_ex.html' # user defined name 'template_name'
    context = {                     # user defined name 'context'
        "name" : "Meher Krishna Patel",
        "fruits" : ["apple", "orange", "banana"]
    }
    return render(request, template_name, context)


# context in class-based views
class  CExample(TemplateView):
    template_name = 'books/context_ex.html'  # 'template_name' is predefined
    def get_context_data(self, *args, **kwargs):
        context = {                     # user defined name 'context'
                "name" : "Harry",
                "fruits" : ["pear", "cherry", "berry"]
            }
        return context




# display list of books using function based views
def book_list(request): 
    template_name = "books/book_list.html"
    queryset = Book.objects.all()
    context = {
        "name" : "Meher Krishna Patel", 
        "object_list": queryset
    }
    return render(request, template_name, context)


# display list of books using class based views
class BookList(ListView): 
    # # default template location : lowercaseAppName/lowercaseAppName_list.html
    # template_name = "books/book_list.html"

    # by default 'queryset' is sent as context 
    queryset = Book.objects.all()  # 'queryset' is reserved name like 'template_name'

    # to pass more context, inherit cotext with 'super' and 
    # then include the items in the context  using 'get_context_data'
    def get_context_data(self, *args, **kwargs): 
        context = super().get_context_data(*args, **kwargs)
        context["name"] = "Harry"
        return context

class AuthorList(ListView): 
    # # default template location : lowercaseAppName/lowercaseAppName_list.html
    # template_name = "books/book_list.html"

    # by default 'queryset' is sent as context 
    queryset = Author.objects.all()  # 'queryset' is reserved name like 'template_name'

    # to pass more context, inherit cotext with 'super' and 
    # then include the items in the context  using 'get_context_data'
    def get_context_data(self, *args, **kwargs): 
        context = super().get_context_data(*args, **kwargs)
        context["name"] = "Harry"
        return context


# book details using class based view
# we can use model=Book in place of queryset
class BookDetail(DetailView):
    model = Book  # use this or below 
    # queryset = Book.objects.all()


# read args using function-based view
def read_arg(request, val):
    template_name = 'books/read_url.html'
    context = {
        "value" : val
    }
    return render(request, template_name, context)

# read kwargs using function-based view
def read_kwarg(request, test):  # test is defined in 'url' (cant be changed)
    template_name = 'books/read_url.html'
    context = {
        "value" : test
    }
    return render(request, template_name, context)

# read args using class-based view
class ReadArg(TemplateView):
    template_name = 'books/read_url.html'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["value"] = self.args[0]
        return context
 
# read kwargs using class-based view
class ReadKwarg(TemplateView):
    template_name = 'books/read_url.html'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        # test is defined in 'url' (cant be changed)
        context["value"] = (self.kwargs.get("test"))
        return context


# Search books 
class SearchBooks(ListView):
    template_name = 'books/book_list.html'
    def get_queryset(self):
        val = self.kwargs.get("urlsearch")
        if val:
            queryset = Book.objects.filter(title__icontains=val)
        else:
            queryset = Book.objects.none()
        return queryset


# Search books using Q
class QSearchBooks(ListView):
    template_name = 'books/book_list.html'
    def get_queryset(self):
        val = self.kwargs.get("qurlsearch")
        if val:
            queryset = Book.objects.filter(
                Q(title__icontains=val) 
              | Q(authors__first_name__icontains=val) # | is or 
            ) # .distinct()  
            # distinct is used, otherwise same book will be listed 
            # twice if book has two authors
            queryset = queryset.distinct()
        else:
            queryset = Book.objects.none()
        return queryset


# link to search form
class  BookSearch(TemplateView):
    template_name = 'books/search_form.html'  # 'template_name' is predefined

class BookSearchResult(ListView):
    model = Book
    template_name = 'books/book_list.html'

    def get_queryset(self, *args, **kwargs):
        val = self.request.GET.get("q")
        if val:
            queryset = Book.objects.filter(
                Q(title__icontains=val) |
                Q(content__icontains=val)
                ).distinct()
        else:
            queryset = Book.objects.none()
        return queryset


###### Forms ##################

@login_required() # LOGIN_URL = '/login/' in settings.py
# '/login/' is the url-name' saved in urls.py in configuration-folder
# or use below, 
# @ login_required(login_url='/login/')
def author_create(request):
    form = AuthorForm(request.POST or None)
    error = None
    if form.is_valid() and request.user.is_staff:
        # # use below to save
        # form.save() 
        # #  or below if want to perform some operations before saving
        new_item = form.save(commit=False)
        # if 
        context ={
            "item" : "Author", 
            "title" : new_item.first_name
        }
        new_item.save()
        template_name = 'books/thanks_create.html'
        return render(request, template_name, context)
    else:
        if not request.user.is_staff:
            errors = "Only staff can add data!"
        else:
            errors = form.errors
        template_name = 'books/create_form.html'
        context = {
                "form" : form, 
                "errors" : errors, 
                "item" : "Author"
            }
        return render(request, template_name, context)


class AuthorCreate(PermissionRequiredMixin, CreateView):
    permission_required = 'user.is_staff'
    form_class = AuthorForm
    template_name = 'books/create_form.html'
    success_url = "/books/authorlist/"

    def form_valid(self, form):
        new_item = form.save(commit=False)
        # do something here
        return super().form_valid(form)

    def get_context_data(self, *args, **kwargs): # heading for Form
        context = super().get_context_data(*args, **kwargs)
        context["item"] = "Author"
        return context


class BookUpdate(LoginRequiredMixin, UpdateView):
    template_name = "books/book_update.html"
    form_class = BookForm

    def get_queryset(self): # if staff allow all book updates
        user = self.request.user
        if user.is_staff:
            queryset = Book.objects.all()
        else: # filter books with username = Author first name
            queryset = Book.objects.filter(authors__first_name__icontains=user)
        return queryset